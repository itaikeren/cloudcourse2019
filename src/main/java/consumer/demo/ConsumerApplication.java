package consumer.demo;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.web.client.RestTemplate;

public class ConsumerApplication {

	public static void main(String[] args) {
		String url = "http://localhost:8095/message";
		RestTemplate restTemplate = new RestTemplate();
		
		try {
			System.out.println("creating new message...");
			Object newMessage = Collections.singletonMap("message", "test");
			String ouputJson = restTemplate.postForObject(url, newMessage, String.class);
			System.out.println(ouputJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String no = "5";
		try {
			System.out.println("updating message #" + no);
			Map<String, Object> object = Collections.singletonMap("message", "updated message");
			restTemplate.put(url + "/{id}", object, no);
			
			System.out.println("successfully updated message #" + no);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		try {
			System.out.println("\nretrieving message #" + no);
			Map<?,?> message = restTemplate.getForObject(url + "/{id}", Map.class, no);
			System.out.println("message retrieved: " + message);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			System.out.println("deleting message #" + no);
			restTemplate.delete(url + "/{id}", no);
			
			System.out.println("successfully deleted message #" + no);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			int size = 3;
			int page = 1;
			System.out.println("\nretrieving " + size + "  messages from page #" + (page + 1));
			List<?> messages = restTemplate.getForObject(url + "?size={size}&page={page}", List.class, size, page);
			System.out.println("messages retrieved: ");
			messages
				.forEach(System.out::println);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
