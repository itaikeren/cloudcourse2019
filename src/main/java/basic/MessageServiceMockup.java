package basic;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

//@Service
public class MessageServiceMockup implements MessageService {

	@Override
	public Message getMessage(String id) {
		try {
			Long theId = Long.parseLong(id);
			if (theId <= 0) {
				throw new MessageNotFoundException ("no message with id: " + id);
			}
			Message rv = new Message("dummy message");
			rv.setId(theId);
			return rv;
		} catch (NumberFormatException e) {
			throw new MessageNotFoundException ("illegal id: " + id);
		}
	}

	@Override
	public List<Message> getAllMessages(int size, int page) {
		if (size <= 0) {
			throw new RuntimeException("size must be postive");
		}

		if (page < 0) {
			throw new RuntimeException("page must be non negative");
		}

		return  
			IntStream.range(1, 1000) // Stream of integers 1-1000
				.skip(size * page)
				.limit(size)
				.mapToObj(i->{
					Message msg = new Message("message #" + i);
					msg.setId((long)i);
					return msg;
				}) // Stream of Message
				.collect(Collectors.toList()); // List<Message>
	}

	@Override
	public Message createMessage(Message newMessage) {
		if (newMessage.getMessage() == null || newMessage.getMessage().trim().isEmpty()) {
			throw new RuntimeException("new message must contain content");
		}
		newMessage.setId(5L);
		return newMessage;
	}

	@Override
	public Message update(String id, Message update) {
		try {
			Long idNumber = Long.parseLong(id);
			if (idNumber < 0) {
				throw new MessageNotFoundException ("message not found");
			}
			return update;
		} catch (NumberFormatException e) {
			throw new MessageNotFoundException ("illegal id: " + id);
		}
	}

	@Override
	public Message delete(String id) {
		try {
			Long idNumber = Long.parseLong(id);
			if (idNumber < 0) {
				throw new MessageNotFoundException ("message not found");
			}
			Message deletedMessage = new Message("old message");
			deletedMessage.setId(idNumber);
			return deletedMessage;
		} catch (NumberFormatException e) {
			throw new MessageNotFoundException ("illegal id: " + id);
		}
	}

}
