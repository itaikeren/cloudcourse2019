package basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class BasicMessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicMessagingApplication.class, args);
	}

}
