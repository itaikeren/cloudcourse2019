package basic;

import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
	private MessageService messages;

//	@Autowired
	public MessageController(MessageService messages) {
		super();
		this.messages = messages;
	}
	
	@RequestMapping(path = "/message/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Message getMessage (@PathVariable("id") String id) {
		return this.messages.getMessage(id);
	}
	
	@RequestMapping(path = "/message", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Message[] getAllMessages (
			@RequestParam(name="size", required = false, defaultValue = "20") int size, 
			@RequestParam(name="page", required = false, defaultValue = "0") int page) {
		return this.messages
				.getAllMessages(size, page)
				.toArray(new Message[0]);
	}
	
	@RequestMapping(path = "/message", method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Message createMessage (@RequestBody Message newMessage) {
		return this.messages.createMessage(newMessage);
	}
	
	@RequestMapping(path = "/message/{id}", method = RequestMethod.PUT, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void update (
			@PathVariable("id") String id, 
			@RequestBody Message update) {
		this.messages.update(id, update);
	}
	
	@RequestMapping(path = "/message/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete (@PathVariable("id") String id) {
		this.messages.delete(id);
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String, String> handleMessageNotFound (MessageNotFoundException e){
		String message = e.getMessage();
		if (message == null) {
			message = "Message not found";
		}
		return Collections.singletonMap("error", message);
	}
	
	
}




















