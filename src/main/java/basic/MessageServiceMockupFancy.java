package basic;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

@Service
public class MessageServiceMockupFancy implements MessageService {

	private Map<String, Message> db = Collections.synchronizedMap(new HashMap<String, Message>());

	@Override
	public Message getMessage(String id) {
		try {
			if (!db.containsKey(id)) {
				throw new MessageNotFoundException("no message with id: " + id);
			}
			Message rv = db.get(id);
			rv.setId(Long.parseLong(id));
			return rv;
		} catch (NumberFormatException e) {
			throw new MessageNotFoundException("illegal id: " + id);
		}
	}

	@Override
	public List<Message> getAllMessages(int size, int page) {
		if (size <= 0) {
			throw new RuntimeException("size must be postive");
		}

		if (page < 0) {
			throw new RuntimeException("page must be non negative");
		}

		return IntStream.range(1, db.size() + 1) // Stream of integers 1-map size
				.skip(size * page)
				.limit(size)
				.mapToObj(i -> {
					Message msg = db.get(i + "");
					msg.setId((long) i);
					return msg;
				}) // Stream of Message
				.collect(Collectors.toList()); // List<Message>
	}

	@Override
	public Message createMessage(Message newMessage) {
		if (newMessage.getMessage() == null || newMessage.getMessage().trim().isEmpty()) {
			throw new RuntimeException("new message must contain content");
		}
		String nextId = db.size() + 1 + "";
		newMessage.setId(Long.parseLong(nextId));
		db.put(nextId, newMessage);
		return db.get(nextId);
	}

	@Override
	public Message update(String id, Message update) {
		try {
			Long idNumber = Long.parseLong(id);
			if (idNumber < 0) {
				throw new MessageNotFoundException("message not found");
			}
			db.put(id, update);
			return db.get(id);
		} catch (NumberFormatException e) {
			throw new MessageNotFoundException("illegal id: " + id);
		}
	}

	@Override
	public Message delete(String id) {
		try {
			Long idNumber = Long.parseLong(id);
			if (idNumber < 0) {
				throw new MessageNotFoundException("message not found");
			}
			return db.put(id, new Message("Message go deleted"));
		} catch (NumberFormatException e) {
			throw new MessageNotFoundException("illegal id: " + id);
		}
	}

}
