package basic;

public class MessageNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 5998148777628000308L;

	public MessageNotFoundException() {
	}

	public MessageNotFoundException(String message) {
		super(message);
	}

	public MessageNotFoundException(Throwable cause) {
		super(cause);
	}

	public MessageNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
