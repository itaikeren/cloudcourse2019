package basic;

import java.util.List;

public interface MessageService {
	public Message getMessage (String id);
	public List<Message> getAllMessages (int size, int page);
	
	public Message createMessage (Message newMessage);
	public Message update (String id, Message update);
	public Message delete (String id);
}
